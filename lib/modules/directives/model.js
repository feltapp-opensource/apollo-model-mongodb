"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.schemaDirectives = exports.typeDef = exports.MODEL_WITH_EMBEDDED = exports.MULTIPLE_MODEL = void 0;

var _graphqlTools = require("graphql-tools");

var _pluralize = _interopRequireDefault(require("pluralize"));

var _sdlSyntaxException = _interopRequireDefault(require("../../sdlSyntaxException"));

var _utils = require("../../utils");

const MULTIPLE_MODEL = 'multipleModel';
exports.MULTIPLE_MODEL = MULTIPLE_MODEL;
const MODEL_WITH_EMBEDDED = 'modelWithEmbedded';
exports.MODEL_WITH_EMBEDDED = MODEL_WITH_EMBEDDED;
const typeDef = `directive @model(collection:String=null, create:Boolean, delete:Boolean, update:Boolean, query:Boolean) on OBJECT | INTERFACE`;
exports.typeDef = typeDef;

class Model extends _graphqlTools.SchemaDirectiveVisitor {
  visitObject(object) {
    const {
      collection,
      ...mmRest
    } = this.args;
    const {
      _typeMap: SchemaTypes
    } = this.schema;
    object.mmRest = mmRest;
    object.mmCollectionName = collection || (0, _utils.lowercaseFirstLetter)((0, _pluralize.default)(object.name)); //validate usage

    const orgCacheControl = object.astNode.directives.find(d => d.name.value === 'cacheControl');
    let cacheControlDirective = orgCacheControl;

    object._interfaces.forEach(iface => {
      if (iface.astNode.directives.find(d => d.name.value === 'cacheControl') && !cacheControlDirective) {
        cacheControlDirective = iface.astNode.directives.find(d => d.name.value === 'cacheControl');
      }

      if ((0, _utils.getDirective)(iface, 'model')) {
        throw new _sdlSyntaxException.default(`Type '${object.name}' can not be marked with @model directive because it's interface ${iface.name} marked with @model directive`, MULTIPLE_MODEL, [object, iface]);
      }

      if ((0, _utils.getDirective)(iface, 'embedded')) {
        throw new _sdlSyntaxException.default(`Type '${object.name}' can not be marked with @model directive because it's interface ${iface.name} marked with @embedded directive`, MODEL_WITH_EMBEDDED, [object, iface]);
      }
    });

    if (cacheControlDirective && !orgCacheControl) {
      object.astNode.directives.push(cacheControlDirective);
    }
  }

  visitInterface(iface) {
    const {
      collection,
      ...mmRest
    } = this.args;
    iface.mmCollectionName = collection || (0, _utils.lowercaseFirstLetter)((0, _pluralize.default)(iface.name));
    iface.mmRest = mmRest;
    const {
      _typeMap: SchemaTypes
    } = this.schema;
    Object.values(SchemaTypes).filter(type => type._interfaces && type._interfaces.includes(iface)).forEach(type => {
      type.mmCollectionName = iface.mmCollectionName;
      const orgCacheControl = iface.astNode.directives.find(d => d.name.value === 'cacheControl');
      let cacheControlDirective = orgCacheControl; //validate usage

      type._interfaces.filter(i => i !== iface).forEach(i => {
        if (i.astNode.directives.find(d => d.name.value === 'cacheControl') && !cacheControlDirective) {
          cacheControlDirective = i.astNode.directives.find(d => d.name.value === 'cacheControl');
        }

        if ((0, _utils.getDirective)(i, 'model')) {
          throw new _sdlSyntaxException.default(`Type '${type.name}' can not inherit both '${iface.name}' and '${i.name}' because they marked with @model directive`, MULTIPLE_MODEL, [i, iface]);
        }

        if ((0, _utils.getDirective)(i, 'embedded')) {
          throw new _sdlSyntaxException.default(`Type '${type.name}' can not inherit both '${iface.name}' and '${i.name}' because they marked with @model and @embedded directives`, MODEL_WITH_EMBEDDED, [i, iface]);
        }
      });

      if (!orgCacheControl && cacheControlDirective) {
        iface.astNode.directives.push(cacheControlDirective);
      }
    }); // iface.resolveType = doc => {
    //   return iface.mmDiscriminatorMap[doc[iface.mmDiscriminatorField]];
    // };
    ////////////
  }

}

const schemaDirectives = {
  model: Model
};
exports.schemaDirectives = schemaDirectives;
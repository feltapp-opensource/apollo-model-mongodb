"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.schemaDirectives = exports.typeDef = exports.ABSTRACT_WITH_EMBEDDED = exports.ABSTRACT_WITH_MODEL = exports.SHOULD_BE_MODEL = void 0;

var _graphqlTools = require("graphql-tools");

var _utils = require("../../utils");

var _sdlSyntaxException = _interopRequireDefault(require("../../sdlSyntaxException"));

const SHOULD_BE_MODEL = 'shouldBeModel';
exports.SHOULD_BE_MODEL = SHOULD_BE_MODEL;
const ABSTRACT_WITH_MODEL = 'abstractWithModel';
exports.ABSTRACT_WITH_MODEL = ABSTRACT_WITH_MODEL;
const ABSTRACT_WITH_EMBEDDED = 'abstractWithEmbedded';
exports.ABSTRACT_WITH_EMBEDDED = ABSTRACT_WITH_EMBEDDED;
const typeDef = `directive @abstract(from:String = null, create:Boolean, delete:Boolean, update:Boolean, query:Boolean) on INTERFACE`;
exports.typeDef = typeDef;

const _addFromInterfaces = function (iface, type) {
  if (iface.mmFrom) {
    if (!type._interfaces.find(i => i === iface.mmFrom)) {
      type._interfaces.push(iface.mmFrom);

      _addFromInterfaces(iface.mmFrom, type);
    }
  }
};

const setAbstractTypes = iface => {
  if (iface.mmFromAbstract) {
    let types = new Set([...iface.mmFromAbstract.mmAbstractTypes, ...iface.mmAbstractTypes]);
    iface.mmFromAbstract.mmAbstractTypes = Array.from(types);
    setAbstractTypes(iface.mmFromAbstract);
  }
};

class Abstract extends _graphqlTools.SchemaDirectiveVisitor {
  visitInterface(iface) {
    const {
      _typeMap: SchemaTypes
    } = this.schema;
    const {
      from = null,
      ...mmRest
    } = this.args;
    iface.mmRest = mmRest;
    iface.mmAbstract = true;
    iface.mmAbstractTypes = [];

    if (from) {
      let fromAbstract = Object.values(SchemaTypes).find(type => type.name === from);

      if (!fromAbstract) {
        throw `from:${from} was not found or does not contain the abstract directive`;
      }

      iface.mmFromAbstract = fromAbstract.mmInherit ? fromAbstract : null;
      iface.mmFrom = fromAbstract;
      iface._fields = { ...fromAbstract._fields,
        ...iface._fields
      };
    }

    Object.values(SchemaTypes).filter(type => type._interfaces && type._interfaces.includes(iface)).forEach(type => {
      iface.mmAbstractTypes.push(type);

      _addFromInterfaces(iface, type);

      if (!(0, _utils.getDirective)(type, 'model')) {
        throw new _sdlSyntaxException.default(`
            Type '${type.name}' is inherited from abstract interface '${iface.name}' and should be marked with @model directive
          `, SHOULD_BE_MODEL, [type, iface]);
      }

      type._interfaces.filter(i => i !== iface).forEach(i => {
        if ((0, _utils.getDirective)(i, 'model')) {
          throw new _sdlSyntaxException.default(`Type '${type.name}' can not inherit both '${iface.name}' and '${i.name}' because they marked with @abstract and @model directives`, ABSTRACT_WITH_MODEL, [i, iface]);
        }

        if ((0, _utils.getDirective)(i, 'embedded')) {
          throw new _sdlSyntaxException.default(`Type '${type.name}' can not inherit both '${iface.name}' and '${i.name}' because they marked with @abstract and @embedded directives`, ABSTRACT_WITH_EMBEDDED, [i, iface]);
        }
      });

      type._fields = { ...iface._fields,
        ...type._fields
      };
    });

    iface.mmOnSchemaInit = () => {
      Object.values(SchemaTypes).filter(type => Array.isArray(type._interfaces) && type._interfaces.includes(iface)).forEach(type => {
        let impls = this.schema._implementations[iface.name] || [];

        if (!impls.find(i => i.name === type.name)) {
          impls.push(type);
        }

        this.schema._implementations[iface.name] = impls;
      });
    };

    setAbstractTypes(iface);

    iface.resolveType = data => {
      return iface.mmAbstractTypes.find(t => t.mmCollectionName === data['mmCollection']);
    };
  }

}

const schemaDirectives = {
  abstract: Abstract
};
exports.schemaDirectives = schemaDirectives;
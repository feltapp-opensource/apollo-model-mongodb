import { SchemaDirectiveVisitor } from 'graphql-tools';
import { getDirective } from '../../utils';
import SDLSyntaxException from '../../sdlSyntaxException';

export const SHOULD_BE_MODEL = 'shouldBeModel';
export const ABSTRACT_WITH_MODEL = 'abstractWithModel';
export const ABSTRACT_WITH_EMBEDDED = 'abstractWithEmbedded';

export const typeDef = `directive @abstract(from:String = null, create:Boolean, delete:Boolean, update:Boolean, query:Boolean) on INTERFACE`;

const _addFromInterfaces = function(iface, type) {
  if (iface.mmFrom) {
    if (!type._interfaces.find(i => i === iface.mmFrom)) {
      type._interfaces.push(iface.mmFrom);
      _addFromInterfaces(iface.mmFrom,type);
    }
  }
}

const setAbstractTypes = (iface) => {
  if (iface.mmFromAbstract) {
    let types = new Set([
      ...iface.mmFromAbstract.mmAbstractTypes,
      ...iface.mmAbstractTypes,
    ]);
    iface.mmFromAbstract.mmAbstractTypes = Array.from(types);
    setAbstractTypes(iface.mmFromAbstract);
  }
};

class Abstract extends SchemaDirectiveVisitor {
  visitInterface(iface) {
    const { _typeMap: SchemaTypes } = this.schema;
    const { from = null, ...mmRest } = this.args;

    iface.mmRest = mmRest;

    iface.mmAbstract = true;
    iface.mmAbstractTypes = [];

    if (from) {
      let fromAbstract = Object.values(SchemaTypes).find(
        type => type.name === from
      );
      if (!fromAbstract) {
        throw `from:${from} was not found or does not contain the abstract directive`;
      }
      iface.mmFromAbstract = fromAbstract.mmInherit ? fromAbstract : null;
      iface.mmFrom = fromAbstract;
      iface._fields = { ...fromAbstract._fields, ...iface._fields };
    }

    Object.values(SchemaTypes)
      .filter(type => type._interfaces && type._interfaces.includes(iface))
      .forEach(type => {
        iface.mmAbstractTypes.push(type);
        _addFromInterfaces(iface, type);
        if (!getDirective(type, 'model')) {
          throw new SDLSyntaxException(
            `
            Type '${type.name}' is inherited from abstract interface '${
              iface.name
            }' and should be marked with @model directive
          `,
            SHOULD_BE_MODEL,
            [type, iface]
          );
        }

        type._interfaces
          .filter(i => i !== iface)
          .forEach(i => {
            if (getDirective(i, 'model')) {
              throw new SDLSyntaxException(
                `Type '${type.name}' can not inherit both '${
                  iface.name
                }' and '${
                  i.name
                }' because they marked with @abstract and @model directives`,
                ABSTRACT_WITH_MODEL,
                [i, iface]
              );
            }

            if (getDirective(i, 'embedded')) {
              throw new SDLSyntaxException(
                `Type '${type.name}' can not inherit both '${
                  iface.name
                }' and '${
                  i.name
                }' because they marked with @abstract and @embedded directives`,
                ABSTRACT_WITH_EMBEDDED,
                [i, iface]
              );
            }
          });
        type._fields = { ...iface._fields, ...type._fields };
      });

    iface.mmOnSchemaInit = () => {
      Object.values(SchemaTypes)
        .filter(
          type =>
            Array.isArray(type._interfaces) && type._interfaces.includes(iface)
        )
        .forEach(type => {
          let impls = this.schema._implementations[iface.name] || [];
          if (!impls.find(i => i.name === type.name)) {
            impls.push(type);
          }
          this.schema._implementations[iface.name] = impls;
        });
    };

    setAbstractTypes(iface);
    iface.resolveType = data => {
      return iface.mmAbstractTypes.find(
        t => t.mmCollectionName === data['mmCollection']
      );
    };
  }
}

export const schemaDirectives = {
  abstract: Abstract,
};

import { GraphQLBoolean, GraphQLInputObjectType, GraphQLList } from 'graphql';
import { SchemaDirectiveVisitor } from 'graphql-tools';
import { UserInputError } from 'apollo-server';

import * as _ from 'lodash';

import {
  allQueryArgs,
  getDirective,
  getRelationFieldName,
  prepareUpdateDoc,
} from '../../utils';

import {
  DELETE_ONE,
  DISTINCT,
  FIND,
  FIND_IDS,
  INSERT_MANY,
  INSERT_ONE,
  UPDATE_ONE,
} from '../../queryExecutor';

import InputTypes from '../../inputTypes';
import TypeWrap from '../../typeWrap';
import {
  appendTransform,
  applyInputTransform,
  reduceTransforms,
} from '../../inputTypes/utils';
import * as HANDLER from '../../inputTypes/handlers';
import * as KIND from '../../inputTypes/kinds';
import * as Transforms from '../../inputTypes/transforms';
import { DBRef, ObjectID } from 'mongodb';
import inflection from 'inflection';

export const INPUT_CREATE_ONE_RELATION = 'createOneRelation';
export const INPUT_CREATE_MANY_RELATION = 'createManyRelation';
export const INPUT_UPDATE_ONE_RELATION = 'updateOneRelation';
export const INPUT_UPDATE_MANY_RELATION = 'updateManyRelation';
export const INPUT_UPDATE_ONE_REQUIRED_RELATION = 'updateOneRequiredRelation';
export const INPUT_UPDATE_MANY_REQUIRED_RELATION = 'updateManyRequiredRelation';
export const INPUT_UPDATE_MANY_RELATION_UPDATE = 'updateManyRelationUpdateMany';
export const INPUT_UPDATE_MANY_REQUIRED_RELATION_UPDATE =
  'updateManyRequiredRelationUpdateMany';

let queryExecutor = null;
export const setQueryExecutor = q => (queryExecutor = q);

export const typeDef = `directive @relation(field:String="_id", storeField:String=null, skipDefault:Boolean=false ) on FIELD_DEFINITION`;

const dbRef = dbRef => dbRef.toJSON();

const filterIds = removeIds => r => {
  let found = removeIds.find(d => {
    let did = d;
    let rid = r;
    if (d instanceof DBRef) {
      let { $id: dID } = dbRef(d);
      did = dID;
    }

    if (r instanceof DBRef) {
      let { $id: rID } = dbRef(r);
      rid = rID;
    }

    if (rid instanceof ObjectID) {
      rid = rid.toString();
    }

    if (did instanceof ObjectID) {
      did = did.toString();
    }

    return rid === did;
  });
  return !found;
};

class RelationDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field, { objectType }) {
    const { field: relationField, storeField, skipDefault } = this.args;
    let fieldTypeWrap = new TypeWrap(field.type);
    const { _typeMap: SchemaTypes } = this.schema;
    // let isAbstract = fieldTypeWrap.realType().mmAbstract;
    // issue if interface defined after relation
    let isAbstract = getDirective(fieldTypeWrap.realType(), 'abstract');

    // if (
    //   !(
    //     getDirective(fieldTypeWrap.realType(), 'model') ||
    //     fieldTypeWrap.interfaceWithDirective('model') ||
    //     isAbstract
    //   )
    // ) {
    //   throw `Relation field type should be defined with Model directive or Abstract interface. (Field '${
    //     field.name
    //     }' of type '${fieldTypeWrap.realType().name}')`;
    // }
    this.mmObjectType = objectType;
    this.mmFieldTypeWrap = fieldTypeWrap;
    this.mmRelationField = relationField;
    this.mmStoreField =
      storeField ||
      getRelationFieldName(
        fieldTypeWrap.realType().name,
        relationField,
        fieldTypeWrap.isMany()
      );

    field.mmRelation = true;
    field.mmMany = fieldTypeWrap.isMany();
    field.mmRelationField = relationField;
    field.mmStoreField = this.mmStoreField;

    appendTransform(field, HANDLER.TRANSFORM_TO_INPUT, {
      [KIND.ORDER_BY]: field => [],
      [KIND.CREATE]: this._transformToInputCreateUpdate,
      [KIND.UPDATE]: this._transformToInputCreateUpdate,
      [KIND.WHERE]: this._transformToInputWhere,
    });
    field.mmOnSchemaInit = this._onSchemaInit;
    field.mmOnSchemaBuild = this._onSchemaBuild;

    field.resolve = fieldTypeWrap.isMany()
      ? this._resolveMany(field)
      : this._resolveSingle(field);

    let idFieldName = `${inflection.singularize(field.name)}${
      fieldTypeWrap.isMany() ? 'Ids' : 'Id'
    }`;
    if (!this.mmObjectType._fields[idFieldName]) {
      let idField;
      if(Array.isArray(this.mmObjectType._interfaces)) {
        this.mmObjectType._interfaces.forEach(i=>{
          if(!idField && i._fields[idFieldName]) {
            idField = i._fields[idFieldName];
          }
        });
      }
      if(!idField) {
        idField = {
          name: idFieldName,
          isDeprecated: false,
          args: [],
          type: fieldTypeWrap.isMany()
            ? new GraphQLList(SchemaTypes['ObjectID'])
            : SchemaTypes['ObjectID'],
          resolve: parent => {
            const result = parent?.[this.mmStoreField];
            if (!result) {
              return fieldTypeWrap.isMany() ? [] : null;
            }
            return result;
          },
        };
      }
      this.mmObjectType._fields[idFieldName] = idField;
      appendTransform(idField, HANDLER.TRANSFORM_INPUT, {
        [KIND.CREATE]: this._renameTransform(idFieldName, storeField),
        [KIND.UPDATE]: this._renameTransform(idFieldName, storeField),
        [KIND.WHERE]: this._renameTransform(idFieldName, storeField),
      });
    }
  }

  _renameTransform = (fieldName, dbName) => params => {
    let value = params[fieldName];
    return {
      ..._.omit(params, fieldName),
      [dbName]: value,
    };
  };

  _transformToInputWhere = ({ field }) => {
    const { field: relationField } = this.args;
    const { _typeMap: SchemaTypes } = this.schema;
    let {
      mmFieldTypeWrap: fieldTypeWrap,
      mmCollectionName: collection,
      mmStoreField: storeField,
    } = this;

    /*
    reduceTransforms([
            Transforms.fieldInputTransform(field, KIND.WHERE),
            Transforms.transformModifier(''),
          ]),
     */

    let inputType = InputTypes.get(fieldTypeWrap.realType(), KIND.WHERE);
    let modifiers = fieldTypeWrap.isMany() ? ['some', 'none'] : [''];
    let fields = [];
    if (fieldTypeWrap.isMany()) {
      fields.push({
        name: field.name,
        type: inputType,
        mmTransform: async (params, context) => {
          params = params[field.name];
          let value = await queryExecutor({
            type: DISTINCT,
            collection,
            context: context.context,
            selector: await applyInputTransform(context)(params, inputType),
            options: {
              key: relationField,
            },
          });
          // if (!isMany) {
          value = { $in: value };
          // }
          return { [storeField]: value };
        },
      });
    }
    modifiers.forEach(modifier => {
      let fieldName = field.name;
      if (modifier !== '') {
        fieldName = `${field.name}_${modifier}`;
      }
      fields.push({
        name: fieldName,
        type: inputType,
        mmTransform: async (params, context) => {
          params = params[fieldName];
          let value = await queryExecutor({
            type: DISTINCT,
            collection,
            context: context.context,
            selector: await applyInputTransform(context)(params, inputType),
            options: {
              key: relationField,
            },
          });
          // if (!isMany) {
          value = { $in: value };
          // }
          return { [storeField]: value };
        },
      });

      fields.push({
        name: `${field.name}_exists`,
        type: GraphQLBoolean,
        mmTransform: async (params, context) => {
          params = params[`${field.name}_exists`];
          let value = { $exists: params };
          return { [storeField]: value };
        },
      });

      fields.push({
        name: `${field.name}_not`,
        type: SchemaTypes['ObjectID'],
        mmTransform: async (params, context) => {
          params = params[`${field.name}_not`];
          let value = { $not: { $eq: params } };
          return { [storeField]: value };
        },
      });
    });
    return fields;
  };

  _validateInput = (type, isMany) => params => {
    let input = _.head(Object.values(params));
    if (Object.keys(input).length === 0) {
      throw new UserInputError(
        `You should fill any field in ${type.name} type`
      );
    }
    if (!isMany && Object.keys(input).length > 1) {
      throw new UserInputError(
        `You should not fill multiple fields in ${type.name} type`
      );
    }
    return params;
  };

  _groupBy = (input, field) =>
    input.reduce((colls, c) => {
      let parameter = c[field];
      let value = _.omit(c, field);
      return colls[parameter]
        ? {
            ...colls,
            [parameter]: [...colls[parameter], value],
          }
        : {
            ...colls,
            [parameter]: [value],
          };
    }, {});

  _groupByCollection = input => this._groupBy(input, 'mmCollectionName');

  _transformToInputCreateUpdate = ({ field, kind, inputTypes }) => {
    let fieldTypeWrap = new TypeWrap(field.type);
    let isCreate = kind === KIND.CREATE;
    const { _typeMap: SchemaTypes } = this.schema;
    const { mmStoreField: storeField } = this;

    let type = inputTypes.get(
      fieldTypeWrap.realType(),
      fieldTypeWrap.isMany()
        ? isCreate
          ? INPUT_CREATE_MANY_RELATION
          : fieldTypeWrap.isRequired()
          ? INPUT_UPDATE_MANY_REQUIRED_RELATION
          : INPUT_UPDATE_MANY_RELATION
        : isCreate
        ? INPUT_CREATE_ONE_RELATION
        : fieldTypeWrap.isRequired()
        ? INPUT_UPDATE_ONE_REQUIRED_RELATION
        : INPUT_UPDATE_ONE_RELATION
    );
    return [
      {
        name: field.name,
        type,
        mmTransform: reduceTransforms([
          this._validateInput(type, fieldTypeWrap.isMany()),
          Transforms.applyNestedTransform(type),
          fieldTypeWrap.isMany()
            ? this._transformInputMany(isCreate)
            : this._transformInputOne(isCreate),
        ]),
      },
    ];
  };

  _transformInputOne = isCreate => async (params, context) => {
    let { mmStoreField: storeField, mmRelationField: relationField } = this;
    let input = _.head(Object.values(params));
    let collection = this.mmCollectionName;

    if (!isCreate && input.update) {
      let _id = context.parent[storeField];
      if (_id) {
        if (this.isAbstract) {
          let ref = _id.toJSON();
          _id = ref.$id;
          collection = ref.$ref;
        }

        let { doc } = prepareUpdateDoc(input.update);
        await this._updateOneQuery({
          collection,
          selector: { _id },
          doc,
          context,
        });
      }
    }

    if (input.connect) {
      ////Connect
      let selector = input.connect;
      if (this.isAbstract) {
        collection = selector.mmCollectionName;
        delete selector.mmCollectionName;
      }
      let ids = await this._distinctQuery({
        collection,
        selector,
        context: context.context,
      });
      if (ids.length === 0) {
        throw new UserInputError(
          `No records found for selector - ${JSON.stringify(selector)}`
        );
      }
      let id = this.isAbstract
        ? new DBRef(collection, _.head(ids))
        : _.head(ids);
      return { [storeField]: id };
    } else if (input.create) {
      ////Create
      let doc = input.create;
      if (this.isAbstract) {
        let { mmCollectionName: collection, ...doc } = doc;
      }
      let id = await this._insertOneQuery({
        doc,
        collection,
        context,
      });
      id = this.isAbstract ? new DBRef(collection, id) : id;
      return { [storeField]: id };
    } else if (input.disconnect) {
      ////Disconnect
      return {
        [storeField]: { $mmUnset: 1 },
      };
    } else if (input.delete) {
      collection = this.isAbstract ? input.delete.mmCollectionName : collection;
      return {
        [storeField]: {
          $mmDeleteSingleRelation: { collection, relationField },
        },
      };
    }
  };
  _transformInputMany = isCreate => async (params, context) => {
    context.parent = context.parent || {};

    let { mmStoreField: storeField, mmRelationField: relationField  } = this;
    let input = _.head(Object.values(params));

    let ids = context.parent[storeField] || [];
    let disconnect_ids = [];
    let delete_ids = [];
    let connect_ids = [];
    let create_ids = [];

    if (input.connect) {
      ////Connect
      if (this.isAbstract) {
        connect_ids = Promise.all(
          _.toPairs(this._groupByCollection(input.connect)).map(
            ([collection, connects]) =>
              this._distinctQuery({
                selector: { [relationField]: {$in: connects.map(c=>c[relationField])} },
                collection,
                context: context.context,
              }).then(ids => ids.map(id => new DBRef(collection, id)))
          )
        ).then(res => _.flatten(res));
      } else {
        let selector = { [relationField]: {$in: input.connect.map(c=>c[relationField])} };
        connect_ids = this._distinctQuery({
          selector,
          context: context.context,
        });
      }

      // if (ids.length === 0) {
      //   throw new UserInputError(
      //     `No records found for selector - ${JSON.stringify(selector)}`
      //   );
      // }
    }
    if (input.create) {
      ////Create
      let docs = input.create;
      if (this.isAbstract) {
        create_ids = Promise.all(
          _.toPairs(this._groupByCollection(input.create)).map(
            ([collection, creates]) =>
              ////if creates.length>0
              this._insertManyQuery({
                docs: creates,
                context: context.context,
                collection,
              }).then(ids => ids.map(id => new DBRef(collection, id)))
          )
        ).then(res => _.flatten(res));
        // } else {
        //   _ids = await this._insertOneQuery({
        //     doc: creates[0],
        //     context,
        //     collection: coll,
        //   }).then(id => [new DBRef(coll, id)]);
        // }
      } else {
        create_ids = this._insertManyQuery({
          docs,
          context: context.context,
        });
      }
    }
    connect_ids = await connect_ids;
    create_ids = await create_ids;

    if (isCreate) {
      const addedIds = [...connect_ids, ...create_ids].filter(filterIds(ids));
      ids = [...ids, ...addedIds];
    } else {
      const addedIds = [...connect_ids, ...create_ids].filter(filterIds(ids));
      ids = [...ids, ...addedIds];

      if (input.disconnect) {
        if (this.isAbstract) {
          disconnect_ids = Promise.all(
            _.toPairs(this._groupByCollection(input.disconnect)).map(
              ([collection, disconnects]) =>
                this._distinctQuery({
                  selector: { [relationField]: {$in: disconnects.map(c=>c[relationField])} },
                  collection,
                  context: context.context,
                }).then(res => res.map(id => new DBRef(collection, id)))
            )
          ).then(res => _.flatten(res));
        } else {
          ////Disconnect
          let selector = { [relationField]: {$in: input.disconnect.map(c=>c[relationField])} };
          disconnect_ids = await this._distinctQuery({
            selector,
            context: context.context,
          });
        }

        // if (disconnect_ids.length === 0) {
        //   throw new UserInputError(`No records found for where clause`);
        // }
      }
      if (input.delete) {
        if (this.isAbstract) {
          delete_ids = Promise.all(
            _.flatten(
              _.toPairs(this._groupByCollection(input.delete)).map(
                ([collection, deletes]) =>
                  deletes.map(selector =>
                    this._deleteOneQuery({
                      collection,
                      selector,
                      context: context.context,
                    }).then(id => new DBRef(collection, id))
                  )
              )
            )
          );
        } else {
          delete_ids = input.delete.map(async selector =>
            this._deleteOneQuery({ selector, context: context.context })
          );
        }
      }
      disconnect_ids = await Promise.all(disconnect_ids);
      delete_ids = await Promise.all(delete_ids);

      delete_ids = delete_ids.filter(id => id);

      ids = ids.filter(filterIds([...disconnect_ids, ...delete_ids]));
    }

    if (input.updateMany) {
      await Promise.all(
        input.updateMany.map(updateMany => {
          let { where, data } = updateMany;
          let { doc, validations, arrayFilters } = prepareUpdateDoc(data);
          if (Object.keys(validations).length !== 0) {
            where = { $and: [where, validations] };
          }
          return this._updateOneQuery({
            selector: where,
            doc,
            options: { arrayFilters },
            context: context.context,
          });
        })
      );
    }

    if (ids) {
      return { [storeField]: ids };
    } else {
      return {};
    }
  };

  _onSchemaBuild = ({ field }) => {
    let fieldTypeWrap = new TypeWrap(field.type);
    this.mmCollectionName = fieldTypeWrap.realType().mmCollectionName;
    this.mmInterfaceModifier = {};
    this.isAbstract = fieldTypeWrap.isAbstract();
    //Collection name and interface modifier
    if (fieldTypeWrap.interfaceWithDirective('model')) {
      let { mmDiscriminator } = fieldTypeWrap.realType();
      let { mmDiscriminatorField } = fieldTypeWrap.interfaceWithDirective(
        'model'
      );
      this.mmInterfaceModifier = {
        [mmDiscriminatorField]: mmDiscriminator,
      };
    }
  };

  _onSchemaInit = ({ field }) => {
    let fieldTypeWrap = new TypeWrap(field.type);

    ///Args and connection field
    if (fieldTypeWrap.isMany()) {
      let whereType = InputTypes.get(
        fieldTypeWrap.realType(),
        fieldTypeWrap.isInterface() ? KIND.WHERE_INTERFACE : KIND.WHERE
      );
      let orderByType = InputTypes.get(fieldTypeWrap.realType(), KIND.ORDER_BY);

      field.args = allQueryArgs({
        whereType,
        orderByType,
      });

      if (!fieldTypeWrap.isAbstract()) {
        this._addConnectionField(field);
      }
    }
  };

  _resolveSingle = field => async (parent, args, context, info) => {
    const { field: relationField, skipDefault: bypassDefaultQuery } = this.args;
    let {
      mmFieldTypeWrap: fieldTypeWrap,
      mmCollectionName: collection,
      mmStoreField: storeField,
      mmInterfaceModifier,
    } = this;
    let selector = {
      ...mmInterfaceModifier,
    };
    let value = parent[storeField];
    if (!value) return null;

    if (fieldTypeWrap.isAbstract()) {
      let { $id: id, $ref: c } = dbRef(value);
      collection = c;
      value = id;
    }

    return queryExecutor({
      type: FIND_IDS,
      collection,
      selector,
      options: {
        selectorField: relationField,
        ids: [value],
        bypassDefaultQuery,
      },
      context,
    }).then(res => {
      let data = _.head(res);
      if (data) {
        data['mmCollection'] = collection;
      }
      return data;
    });
  };

  _resolveMany = field => async (parent, args, context, info) => {
    const { field: relationField, skipDefault: bypassDefaultQuery } = this.args;
    let {
      mmFieldTypeWrap: fieldTypeWrap,
      mmCollectionName: collection,
      mmObjectType: modelType,
      mmStoreField: storeField,
      mmInterfaceModifier,
    } = this;

    let whereType = InputTypes.get(
      fieldTypeWrap.realType(),
      fieldTypeWrap.isInterface() ? KIND.WHERE_INTERFACE : KIND.WHERE
    );

    let value = parent[storeField];
    if (!value) return fieldTypeWrap.isRequired() ? [] : null;

    let selector = {};
    if (!fieldTypeWrap.isAbstract()) {
      selector = await applyInputTransform({ parent, context, info })(
        args.where,
        whereType
      );
    }

    if (fieldTypeWrap.isInterface()) {
      selector = Transforms.validateAndTransformInterfaceInput(whereType)({
        selector,
      }).selector;
    }

    selector = {
      ...selector,
      ...mmInterfaceModifier,
    };
    if (args.skip) {
      value = _.drop(value, args.skip);
    }
    if (args.first) {
      value = _.take(value, args.first);
    }

    let sort = args.orderBy;
    if (args.sort) {
      sort = args.sort.reduce((a, s) => {
        const [k, v] = Object.entries(s)[0];
        a[k] = v;
        return a;
      }, {});
    }

    if (this.isAbstract) {
      let collections = this._groupBy(value.map(v => v.toJSON()), '$ref');

      return Promise.all(
        _.toPairs(collections).map(([collection, ids]) =>
          this._findIDsQuery({
            collection,
            selector,
            options: {
              selectorField: relationField,
              ids: ids.map(id => id.$id),
              bypassDefaultQuery,
            },
            context: context.context,
          }).then(results =>
            results.map(r => ({
              ...r,
              mmCollectionName: collection,
            }))
          )
        )
      ).then(res => _.flatten(res));
    } else {
      return this._findIDsQuery({
        collection,
        selector,
        options: {
          sort,
          selectorField: relationField,
          ids: value,
          bypassDefaultQuery,
        },
        context: context.context,
      });
    }
  };

  _addConnectionField = field => {
    const { field: relationField } = this.args;
    let {
      mmFieldTypeWrap: fieldTypeWrap,
      mmCollectionName: collection,
      mmStoreField: storeField,
    } = this;
    const { _typeMap: SchemaTypes } = this.schema;

    let whereType = InputTypes.get(fieldTypeWrap.realType(), 'where');
    let orderByType = InputTypes.get(fieldTypeWrap.realType(), 'orderBy');

    let connectionName = `${field.name}Connection`;

    this.mmObjectType._fields[connectionName] = {
      name: connectionName,
      isDeprecated: false,
      args: allQueryArgs({
        whereType,
        orderByType,
      }),
      type: SchemaTypes[`${fieldTypeWrap.realType().name}Connection`],
      resolve: async (parent, args, context, info) => {
        let value = parent[storeField];
        if (Array.isArray(value)) {
          value = { $in: value };
        }
        let selector = {
          $and: [
            await applyInputTransform({ parent, context, info })(
              args.where,
              whereType
            ),
            { [relationField]: value },
          ],
        };
        return {
          _selector: selector,
          _skip: args.skip,
          _limit: args.first,
        };
      },
      [HANDLER.TRANSFORM_TO_INPUT]: {
        [KIND.CREATE]: () => [],
        [KIND.WHERE]: () => [],
        [KIND.UPDATE]: () => [],
        [KIND.ORDER_BY]: () => [],
      },
    };
  };

  _distinctQuery = async ({ collection, selector, context }) => {
    const { field: relationField } = this.args;
    let {
      mmCollectionName,
      mmStoreField: storeField,
      mmInterfaceModifier,
    } = this;
    selector = { ...selector, ...mmInterfaceModifier };
    collection = collection || mmCollectionName;
    return queryExecutor({
      type: DISTINCT,
      collection,
      selector,
      context,
      options: {
        key: relationField,
      },
    });
  };

  _findIDsQuery = async ({ collection, selector, options, context }) => {
    if (options.sort) {
      return queryExecutor({
        type: FIND,
        collection,
        selector: {
          ...selector,
          [options.selectorField]: { $in: options.ids },
        },
        options,
        context,
      });
    } else {
      return queryExecutor({
        type: FIND_IDS,
        collection,
        selector,
        options,
        context,
      });
    }
  };

  _deleteOneQuery = async ({ collection, selector, context }) => {
    const { field: relationField } = this.args;
    let {
      mmCollectionName,
      mmStoreField: storeField,
      mmInterfaceModifier,
    } = this;
    collection = collection || mmCollectionName;
    selector = { ...selector, ...mmInterfaceModifier };

    return queryExecutor({
      type: DELETE_ONE,
      collection,
      selector,
      context,
    }).then(res => (res ? res[relationField] : null));
  };

  _insertOneQuery = async ({ collection, doc, context }) => {
    const { field: relationField } = this.args;
    let {
      mmCollectionName,
      mmStoreField: storeField,
      mmInterfaceModifier,
    } = this;
    doc = { ...doc, ...mmInterfaceModifier };
    collection = collection || mmCollectionName;
    return queryExecutor({
      type: INSERT_ONE,
      collection,
      doc,
      context,
    }).then(res => res[relationField]);
  };

  _insertManyQuery = async ({ collection, docs, context }) => {
    const { field: relationField } = this.args;
    let {
      mmCollectionName,
      mmStoreField: storeField,
      mmInterfaceModifier,
    } = this;
    docs = docs.map(doc => ({ ...doc, ...mmInterfaceModifier }));
    collection = collection || mmCollectionName;
    return queryExecutor({
      type: INSERT_MANY,
      collection,
      docs,
      context,
    }).then(res => res.map(item => item[relationField]));
  };

  _updateOneQuery = async ({ collection, selector, doc, context, ...rest }) => {
    const { field: relationField } = this.args;
    let {
      mmCollectionName,
      mmStoreField: storeField,
      mmInterfaceModifier,
    } = this;
    doc = { ...doc, ...mmInterfaceModifier };
    collection = collection || mmCollectionName;
    return queryExecutor({
      type: UPDATE_ONE,
      selector,
      collection,
      doc,
      context,
    });
  };
}

let createInputTransform = (type, isInterface) =>
  reduceTransforms([
    Transforms.applyNestedTransform(type),
    isInterface ? Transforms.validateAndTransformInterfaceInput(type) : null,
  ]);

const createInput = ({ name, initialType, kind, inputTypes }) => {
  let fields = {};
  let typeWrap = new TypeWrap(initialType);

  let createType = inputTypes.get(
    initialType,
    typeWrap.isInterface() ? KIND.CREATE_INTERFACE : KIND.CREATE
  );
  let whereType = inputTypes.get(
    initialType,
    typeWrap.isInterface() ? KIND.WHERE_INTERFACE : KIND.WHERE
  );

  let updateType = inputTypes.get(
    initialType,
    typeWrap.isInterface() ? KIND.UPDATE_INTERFACE : KIND.UPDATE
  );

  let whereUniqueType = inputTypes.get(
    initialType,
    typeWrap.isInterface() ? KIND.WHERE_UNIQUE_INTERFACE : KIND.WHERE_UNIQUE
  );

  if (
    [
      INPUT_CREATE_MANY_RELATION,
      INPUT_UPDATE_MANY_RELATION,
      INPUT_UPDATE_MANY_REQUIRED_RELATION,
    ].includes(kind)
  ) {
    createType = new GraphQLList(createType);
    whereType = new GraphQLList(whereType);
    whereUniqueType = new GraphQLList(whereUniqueType);
  }
  fields.create = {
    name: 'create',
    type: createType,
    mmTransform: createInputTransform(createType, typeWrap.isInterface()),
  };
  fields.connect = {
    name: 'connect',
    type: whereUniqueType,
    mmTransform: createInputTransform(whereUniqueType, typeWrap.isInterface()),
  };

  if (
    [INPUT_UPDATE_MANY_RELATION, INPUT_UPDATE_MANY_REQUIRED_RELATION].includes(
      kind
    )
  ) {
    let updateKind = INPUT_UPDATE_MANY_RELATION
      ? INPUT_UPDATE_MANY_RELATION_UPDATE
      : INPUT_UPDATE_MANY_REQUIRED_RELATION_UPDATE;
    let updateManyType = new GraphQLList(
      inputTypes.get(initialType, updateKind)
    );
    fields.updateMany = {
      name: 'updateMany',
      type: updateManyType,
      mmTransform: createInputTransform(updateManyType, typeWrap.isInterface()),
    };
  } else if (
    [INPUT_UPDATE_ONE_RELATION, INPUT_UPDATE_ONE_REQUIRED_RELATION].includes(
      kind
    )
  ) {
    fields.update = {
      name: 'update',
      type: updateType,
      mmTransform: createInputTransform(updateType, typeWrap.isInterface()),
    };
  }

  if (
    [INPUT_UPDATE_MANY_RELATION, INPUT_UPDATE_MANY_REQUIRED_RELATION].includes(
      kind
    )
  ) {
    fields.disconnect = {
      name: 'disconnect',
      type: whereUniqueType,
      mmTransform: createInputTransform(
        whereUniqueType,
        typeWrap.isInterface()
      ),
    };

    fields.delete = {
      name: 'delete',
      type: whereUniqueType,
      mmTransform: createInputTransform(
        whereUniqueType,
        typeWrap.isInterface()
      ),
    };
  }

  if ([INPUT_UPDATE_ONE_RELATION].includes(kind)) {
    fields.disconnect = {
      name: 'disconnect',
      type: GraphQLBoolean,
    };

    fields.delete = {
      name: 'delete',
      type: GraphQLBoolean,
    };
  }

  let newType = new GraphQLInputObjectType({
    name,
    fields,
  });
  newType.getFields();
  return newType;
};
const createUpdateManyInput = ({ name, initialType, kind, inputTypes }) => {
  let fields = {};
  let typeWrap = new TypeWrap(initialType);

  let updateType = inputTypes.get(
    initialType,
    typeWrap.isInterface() ? KIND.UPDATE_INTERFACE : KIND.UPDATE
  );

  let whereUniqueType = inputTypes.get(
    initialType,
    typeWrap.isInterface() ? KIND.WHERE_UNIQUE_INTERFACE : KIND.WHERE_UNIQUE
  );
  fields.where = {
    name: 'where',
    type: whereUniqueType,
    mmTransform: createInputTransform(whereUniqueType, typeWrap.isInterface()),
  };

  fields.data = {
    name: 'data',
    type: updateType,
    mmTransform: createInputTransform(updateType, typeWrap.isInterface()),
  };
  let newType = new GraphQLInputObjectType({
    name,
    fields,
  });
  newType.getFields();
  return newType;
};
InputTypes.registerKind(INPUT_CREATE_ONE_RELATION, createInput);
InputTypes.registerKind(INPUT_CREATE_MANY_RELATION, createInput);
InputTypes.registerKind(INPUT_UPDATE_ONE_RELATION, createInput);
InputTypes.registerKind(INPUT_UPDATE_MANY_RELATION, createInput);
InputTypes.registerKind(INPUT_UPDATE_ONE_REQUIRED_RELATION, createInput);
InputTypes.registerKind(INPUT_UPDATE_MANY_REQUIRED_RELATION, createInput);
InputTypes.registerKind(
  INPUT_UPDATE_MANY_RELATION_UPDATE,
  createUpdateManyInput
);
InputTypes.registerKind(
  INPUT_UPDATE_MANY_REQUIRED_RELATION_UPDATE,
  createUpdateManyInput
);

export const schemaDirectives = {
  relation: RelationDirective,
};

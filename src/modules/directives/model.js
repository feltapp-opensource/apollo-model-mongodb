import { SchemaDirectiveVisitor } from 'graphql-tools';
import pluralize from 'pluralize';

import SDLSyntaxException from '../../sdlSyntaxException';
import { getDirective, lowercaseFirstLetter } from '../../utils';

export const MULTIPLE_MODEL = 'multipleModel';
export const MODEL_WITH_EMBEDDED = 'modelWithEmbedded';

export const typeDef = `directive @model(collection:String=null, create:Boolean, delete:Boolean, update:Boolean, query:Boolean) on OBJECT | INTERFACE`;

class Model extends SchemaDirectiveVisitor {
  visitObject(object) {
    const { collection, ...mmRest } = this.args;
    const { _typeMap: SchemaTypes } = this.schema;
    object.mmRest = mmRest;
    object.mmCollectionName =
      collection || lowercaseFirstLetter(pluralize(object.name));

    //validate usage
    const orgCacheControl = object.astNode.directives.find(
      d => d.name.value === 'cacheControl'
    );
    let cacheControlDirective = orgCacheControl;

    object._interfaces.forEach(iface => {
      if (
        iface.astNode.directives.find(d => d.name.value === 'cacheControl') &&
        !cacheControlDirective
      ) {
        cacheControlDirective = iface.astNode.directives.find(
          d => d.name.value === 'cacheControl'
        );
      }
      if (getDirective(iface, 'model')) {
        throw new SDLSyntaxException(
          `Type '${
            object.name
          }' can not be marked with @model directive because it's interface ${
            iface.name
          } marked with @model directive`,
          MULTIPLE_MODEL,
          [object, iface]
        );
      }
      if (getDirective(iface, 'embedded')) {
        throw new SDLSyntaxException(
          `Type '${
            object.name
          }' can not be marked with @model directive because it's interface ${
            iface.name
          } marked with @embedded directive`,
          MODEL_WITH_EMBEDDED,
          [object, iface]
        );
      }
    });
    if (cacheControlDirective && !orgCacheControl) {
      object.astNode.directives.push(cacheControlDirective);
    }
  }

  visitInterface(iface) {
    const { collection, ...mmRest } = this.args;
    iface.mmCollectionName =
      collection || lowercaseFirstLetter(pluralize(iface.name));
    iface.mmRest = mmRest;
    const { _typeMap: SchemaTypes } = this.schema;
    Object.values(SchemaTypes)
      .filter(type => type._interfaces && type._interfaces.includes(iface))
      .forEach(type => {
        type.mmCollectionName = iface.mmCollectionName;
        const orgCacheControl = iface.astNode.directives.find(
          d => d.name.value === 'cacheControl'
        );
        let cacheControlDirective = orgCacheControl;
        //validate usage
        type._interfaces
          .filter(i => i !== iface)
          .forEach(i => {
            if (
              i.astNode.directives.find(d => d.name.value === 'cacheControl') &&
              !cacheControlDirective
            ) {
              cacheControlDirective = i.astNode.directives.find(
                d => d.name.value === 'cacheControl'
              );
            }
            if (getDirective(i, 'model')) {
              throw new SDLSyntaxException(
                `Type '${type.name}' can not inherit both '${
                  iface.name
                }' and '${i.name}' because they marked with @model directive`,
                MULTIPLE_MODEL,
                [i, iface]
              );
            }
            if (getDirective(i, 'embedded')) {
              throw new SDLSyntaxException(
                `Type '${type.name}' can not inherit both '${
                  iface.name
                }' and '${
                  i.name
                }' because they marked with @model and @embedded directives`,
                MODEL_WITH_EMBEDDED,
                [i, iface]
              );
            }
          });
        if (!orgCacheControl && cacheControlDirective) {
          iface.astNode.directives.push(cacheControlDirective);
        }
      });

    // iface.resolveType = doc => {
    //   return iface.mmDiscriminatorMap[doc[iface.mmDiscriminatorField]];
    // };
    ////////////
  }
}

export const schemaDirectives = {
  model: Model,
};
